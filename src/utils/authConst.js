export const IDENTITY_CONFIG = {
  authority: process.env.REACT_APP_AUTH_URL,
  client_id: process.env.REACT_APP_IDENTITY_CLIENT_ID,
  redirect_uri: process.env.REACT_APP_REDIRECT_URL,
  silent_redirect_uri: process.env.REACT_APP_SILENT_REDIRECT_URL,
  post_logout_redirect_uri: process.env.REACT_APP_LOGOFF_REDIRECT_URL,
  audience: "your audience",
  response_type: "id_token token",
  automaticSilentRenew: false,
  loadUserInfo: true,
  scope: "openid"
};

export const METADATA_OIDC = {
  issuer: "account.trustmaster.org",
  jwks_uri:process.env.REACT_APP_AUTH_URL + "/.well-known/jwks",
  authorization_endpoint: process.env.REACT_APP_AUTH_URL + "/oauth2/authorize",
  token_endpoint: process.env.REACT_APP_AUTH_URL + "/oauth2/token",
  userinfo_endpoint: process.env.REACT_APP_AUTH_URL + "/oauth2/userinfo",
  end_session_endpoint: process.env.REACT_APP_AUTH_URL + "/oauth2/logout?client_id=" + process.env.REACT_APP_IDENTITY_CLIENT_ID,
  check_session_iframe: process.env.REACT_APP_AUTH_URL + "/oauth2/userinfo",
  revocation_endpoint: process.env.REACT_APP_AUTH_URL + "/oauth2/logout?client_id=" + process.env.REACT_APP_IDENTITY_CLIENT_ID,
  introspection_endpoint: process.env.REACT_APP_AUTH_URL + "/oauth2/introspect"
};