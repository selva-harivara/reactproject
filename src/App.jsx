import React, {Component, Suspense } from "react";
import { AuthProvider } from "./providers/authProvider";
import { BrowserRouter } from "react-router-dom";
//import { Route, Switch } from 'react-router-dom';

import {Routes} from "./routes/routes";
// import { Layout } from './components/Layout';
// import { NotfoundPage } from "./components/notfoundPage";
// import { PublicPage } from "./components/publicPage";
//import { UserManager } from "oidc-client";

import './i18n';


/*
    https://www.youtube.com/watch?v=tOK9l5uP06U
    https://github.com/Franpastoragusti/oidc-react-app

    other: https://medium.com/@ricklee_10931/react-multi-lingual-with-react-i18next-57879f986168
    other: https://github.com/maxmantz/redux-oidc-example

    */




export class App extends Component {
    render() {
        return (
            <Suspense fallback="loading">
                <AuthProvider>
                    <BrowserRouter children={Routes} basename={"/"} />
                </AuthProvider>
            </Suspense>
        );
    }
    
}
