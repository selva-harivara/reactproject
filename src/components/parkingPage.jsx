import React from 'react';
import { NavigationBar } from './NavigationBar';
import { Container } from 'react-bootstrap';

//import { Jumbotron } from './Jumbotron';
import { useTranslation } from 'react-i18next'

import '.././i18n';







export const ParkingPage = () => {
    const { t, i18n } = useTranslation();
    const changeLanguage = lng => {
        i18n.changeLanguage(lng);
      };
    return (
        <React.Fragment>
            <NavigationBar/>
            <Container>
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <span class="display-1 d-block">{t('parking.title')}</span>
                        <div class="mb-4 lead">{t('parking.description')}</div>
                        
                    </div>
                </div>
                <button onClick={() => changeLanguage('de')}>de</button>
                <button onClick={() => changeLanguage('en')}>en</button>
                <button onClick={() => changeLanguage('nl')}>nl</button>  
            </Container>   
        </React.Fragment>
    );
};
