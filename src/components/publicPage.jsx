import React from 'react';
import { NavigationBar } from './NavigationBar';
import { Container } from 'react-bootstrap';

import { Jumbotron } from './Jumbotron';
import { useTranslation } from 'react-i18next'

import '.././i18n';







export const PublicPage = () => {
    const { t, i18n } = useTranslation();
    const changeLanguage = lng => {
        i18n.changeLanguage(lng);
      };
    return (
        <React.Fragment>
            <NavigationBar/>
            <Jumbotron />
            <Container>
                Public page
                {t('hello.label')}
            </Container> 
            <button onClick={() => changeLanguage('de')}>de</button>
            <button onClick={() => changeLanguage('en')}>en</button>
            <button onClick={() => changeLanguage('nl')}>nl</button>   
        </React.Fragment>
    );
};
