import React from 'react';
import { NavigationBar } from './../NavigationBar';
import { Container } from 'react-bootstrap';


import { useTranslation } from 'react-i18next'
//import './i18n';

export const e404 = () => {
    const { t, i18n } = useTranslation();
    const changeLanguage = lng => {
        i18n.changeLanguage(lng);
      };
    return (
        <React.Fragment>
            <NavigationBar/>
                <Container>
                <div className="row justify-content-center">
                    <div className="col-md-12 text-center">
                        <span className="display-1 d-block">404</span>
                        <div className="mb-4 lead">{t('error404.description')}</div>
                        <a href="/#" className="btn btn-link">{t('error404.back')}</a>
                    </div>
                </div>
                <button onClick={() => changeLanguage('de')}>de</button>
            <button onClick={() => changeLanguage('en')}>en</button>
            <button onClick={() => changeLanguage('nl')}>nl</button>  
            </Container>   
        </React.Fragment>
    );
};
