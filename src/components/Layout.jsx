import React from 'react';
import { NavigationBar } from './NavigationBar';
import { Jumbotron } from './Jumbotron';
import { Route, Switch } from "react-router-dom";

import { PublicPage } from "../components/publicPage";


export const Layout = (props) => (
  <React.Fragment>
    
    <NavigationBar />
    <Jumbotron />
    <Switch>
      

      <Route exact={true} path="/contact" component={PublicPage} />
      
      <Route exact={true} path="/privacy" component={PublicPage} />
      <Route exact={true} path="/tos" component={PublicPage} />
      
      <Route exact={true} path="/" component={PublicPage} />
    </Switch>
  </React.Fragment>
)

