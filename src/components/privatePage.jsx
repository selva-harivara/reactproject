import React from 'react';
import { NavigationBar } from './NavigationBar';
import { Container } from 'react-bootstrap';

import { Jumbotron } from './Jumbotron';

export const PrivatePage = (props) => {
    return (
        <React.Fragment>
            <NavigationBar/>
            <Jumbotron />
            <Container>
                Private page    
            </Container>   
            <a href="https://gitrepository.nl/jerryhopper/react-parking-page/commits/master"><img alt="pipeline status" src="http://gitrepository.nl/jerryhopper/react-parking-page/badges/master/pipeline.svg" /></a>
    
             
        </React.Fragment>
    );
};
