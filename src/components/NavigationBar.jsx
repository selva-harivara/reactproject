import React  from 'react';
import { Link } from 'react-router-dom';
//import { Nav, Navbar,Form,FormControl,Button } from 'react-bootstrap';

import { Nav, Navbar } from 'react-bootstrap';
import styled from 'styled-components';
//import Routes from '../routes/routes'
import {Routes} from "../routes/routes";
//import { AuthConsumer } from "../providers/authProvider";
//import { render } from 'react-dom';
//import UserManager from 'oidc-client';
//import { useTranslation, withTranslation, Trans } from 'react-i18next';
import { Trans } from 'react-i18next';


const Styles = styled.div`
  .navbar {
    background-color: #222;
  }

  a, .navbar-brand, .navbar-nav .nav-link {
    color: #bbb;

    &:hover {
      color: white;
    }
  }
  .navbar-toggler-icon{
    color: white;
  }
`;


function test(i){
  //console.warn(Routes.props.children[i].props.title)
  if( Routes.props.children[i].props.title ){
    return <Nav.Item key={i}><Link to={Routes.props.children[i].props.path}  className="nav-link" role="button">{Routes.props.children[i].props.title}</Link></Nav.Item>                  
    //return <Nav.Item><Link to="/"  className="nav-link" role="button">{Routes.props.children[i].props.path}</Link></Nav.Item>
  }
  
}

export const NavigationBar = (props) => (
  <Styles>
    <Navbar expand="lg">
      <Navbar.Brand href="/">
        <Trans i18nKey="language.label">
          ?
        </Trans>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          { Routes.props.children.map((todo,i)=>( 
            test(i)
            ))}  
        </Nav>
      </Navbar.Collapse>
      
    </Navbar>
    
  </Styles>
);

