import * as React from "react";
import { Switch } from "react-router-dom";

import { Callback } from "../components/auth/callback";
import { Logout } from "../components/auth/logout";
import { LogoutCallback } from "../components/auth/logoutCallback";
import { PrivateRoute } from "./privateRoute";
import { PublicRoute } from "./publicRoute";
import { Register } from "../components/auth/register";
import { SilentRenew } from "../components/auth/silentRenew";
import { PublicPage } from "../components/publicPage";
import { PrivatePage } from "../components/privatePage";
import { ParkingPage } from "../components/parkingPage";
//import { NotfoundPage } from "../components/notfoundPage";
import { e404 } from "../components/error/e404";



export const Routes = (
    <Switch>
      <PublicRoute exact={true} path="/:lng(en|nl|de)" component={PublicPage} />
      
      <PublicRoute exact={true} path="/signin-oidc" component={Callback} />
      <PublicRoute exact={true} path="/logout/callback" component={LogoutCallback} />
      <PublicRoute exact={true} path="/:lng(en|nl|es|de|fr|pt|it)/register/:form?" component={Register} />
      <PublicRoute exact={true} path="/silentrenew" component={SilentRenew} />
      

      
      <PrivateRoute notitle="other" exact={true} path="/private" component={PrivatePage} />
      <PrivateRoute notitle="private" exact={true} path="/private/dashboard" component={PrivatePage} />

      <PublicRoute title="Home" exact={true} path="/" component={ParkingPage} />
      <PrivateRoute title="login" exact={true} path="/login" component={PrivatePage} />
      <PublicRoute notitle="logout" exact={true} path="/logout" component={Logout} />
      
      <PublicRoute component={e404} status={404}/>
    </Switch>  
);
