import React from "react";
import { Route } from "react-router-dom";
import { AuthConsumer } from "../providers/authProvider";



export const PrivateRoute = ({ component, ...rest }) => {
  const renderFn = Component => props => (
    <AuthConsumer>
      {({ isAuthenticated, signinRedirect }) => {
        if (!!Component && isAuthenticated()) {
          return <Component private={true}  {...props} />;
        } else {
          signinRedirect();
          return <span>loading</span>;
        }
      }}
    </AuthConsumer>
  );
  //console.warn(...rest);
  return <Route  {...rest} render={renderFn(component)} />;
};
