import React from "react";
import { Route } from "react-router-dom";
import { AuthConsumer } from "../providers/authProvider";

export const PublicRoute = ({ component, ...rest }) => {
  
  const renderFn = Component => props => (
    
    <AuthConsumer>
      {({ isAuthenticated, signinRedirect }) => {
        return <Component private={false}  {...props} />;  
        //if (!!Component && isAuthenticated()) {
          //return <Component private={false}  {...props} />;
        //} else {
          //signinRedirect();
          //return <span>loading</span>;
        //}
      }}
    </AuthConsumer>
  );
  //console.warn(...rest);
  return <Route  {...rest} render={renderFn(component)} />;
};
