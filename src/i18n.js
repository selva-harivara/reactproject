import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import { initReactI18next } from 'react-i18next';
//import { reactI18nextModule } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import ReduxDetector from 'i18next-redux-languagedetector';

const LngDetector = new LanguageDetector();
LngDetector.addDetector(ReduxDetector);

i18n
  .use(Backend)
  .use(LngDetector)
  .use(initReactI18next)
  .init({
    //lng: 'nl',
    backend: {
      /* translation file path */
      loadPath: '/assets/i18n/{{ns}}/{{lng}}.json'
    },
    detection: {
      // order and from where user language should be detected
      // order: ['querystring', 'cookie', 'localStorage', 'navigator', 'htmlTag', 'path', 'subdomain'],
      order: [
        'path', 'localStorage'
      ],

      // keys or params to lookup language from
      // lookupQuerystring: 'lng',
      //lookupCookie: 'i18next',
      //lookupLocalStorage: 'i18nextLng',
      // lookupFromPathIndex: 0,
      // lookupFromSubdomainIndex: 0,

      //lookupRedux: redux.lookupRedux,
      //cacheUserLanguageRedux: redux.cacheUserLanguageRedux,

      // cache user language on
      // caches: ['localStorage', 'cookie'],
      caches: ['localStorage'],
      //excludeCacheFor: ['cimode'], // languages to not persist (cookie, localStorage)

      // optional expire and domain for set cookie
      // cookieMinutes: 10,
      // cookieDomain: 'myDomain',

      // optional htmlTag with lang attribute, the default is:
      // htmlTag: document.documentElement
    },
    fallbackLng: 'en',
    debug: true,
    /* can have multiple namespace, in case you want to divide a huge translation into smaller pieces and load them on demand */
    ns: ['translations'],
    defaultNS: 'translations',
    keySeparator: false,
    interpolation: {
      escapeValue: false,
      formatSeparator: ','
    },
    react: {
      wait: true
    }
  })

export default i18n;
